PROJ:=template m1 m2 m3 m4 m5

.PHONY: $(PROJ)

all: $(PROJ)

$(PROJ):
	ln -srf theme/fibeamer $(@)/fibeamer
	ln -srf theme/beamerthemefibeamer.sty $(@)/beamerthemefibeamer.sty
	make -C $(@)
