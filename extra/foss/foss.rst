################
FOSS e GNU/Linux
################

.. |arrow| replace:: →

.. |up| replace:: ↑
.. |down| replace:: ↓

.. role:: cover

.. space:: 150

.. class:: cover

    07/05/2021

.. raw:: pdf

    PageBreak standardPage 


Cultura hacker
==============

* Anos 60: computadores distribuídos com código fonte de compiladores e sistemas operacionais

.. image:: img/eniac.png
    :width: 65%


Cultura hacker
==============

* Computadores faziam parte de centros de pesquisa
* Universidades: disseminação de conhecimento
* Códigos corrigidos, modificados, compartilhados publicamente


Copyright de software
=====================

* Até 1974, softwares não poderiam ser protegidos por direitos autorais
* Até então, custo de produção do software embutido no preço de venda do hardware
* Softwares vão se tornando cada vez mais complexos e custosos para escrever e atualizar
* Algumas empresas passam a limitar a redistribuição de seus softwares e deixam de distribuir códigos-fonte


Sistema operacional Unix
========================

* AT&T desenvolve um sistema operacional

    * Para ser utilizado internamente

.. image:: img/unix.png
    :width: 75%


Sistema operacional Unix
========================

* Nos anos 70, cópias foram distribuídas gratuitamente para centros de pesquisa

    * Com restrições relacionadas à modificação e redistribuição

* Sistema era relativamente simples e flexível
* No início dos anos 80 o sistema já era muito popular
* AT&T para de distribuir o sistema gratuitamente, e passa a cobrar também por atualizações


Problemas com o Unix
====================

* Custo do Unix era elevado

    * E nem sempre as atualizações da AT&T eram as que os usuários precisavam

* Era difícil alternar para outra arquitetura

    * Ecossistema maduro

    * Diversos programas escritos para rodar no Unix


Problemas com o Unix
====================

* Software proprietário trazendo problemas

    * Para os centros de pesquisa

    * Para a cultura hacker


Resistência ao software proprietário
====================================

* Pessoas habituadas com a cultura hacker se preocuparam com a tendência proprietária
* Richard Stallman, estudante do MIT

    * Fundou o projeto GNU

    * Fundou a FSF


GNU (GNU is Not Unix)
=====================

* Fundado em 1983
* Objetivo: permitir que as pessoas pudessem usar o computador apenas com software livre
* Atividades:

    * Escrita de softwares livres para o Unix

    * Escrita de um sistema operacional baseado no Unix

.. image:: img/gnu.png
    :width: 10%


FSF (Free Software Foundation)
==============================

* Criada para dar bases legais ao GNU
* Criação das licenças copyleft

    * Copyleft = tipo de licença que utiliza das leis de copyright para combater o copyright
    * Objetivo: garantir que o software livre continue livre
    * GPL

.. image:: img/fsf.png
    :width: 15%


GPL (GNU General Public License)
================================

* Garantias:
    * executar o programa para qualquer propósito
    * estudar e modificar o programa conforme necessário (requer acesso ao código fonte)
    * redistribuir o programa
    * compartilhar modificações com a comunidade

* Sua versão atual é a GPLv3

.. image:: img/gpl.png
    :width: 10%


Richard Stallman na atualidade
==============================

* Figura polêmica
* O movimento de software livre hoje é muito maior que uma única pessoa ou instituição


A peça final do GNU
===================

* Muitos programas escritos para o GNU
* Em 1992, a parte faltante era o núcleo do sistema operacional

    * Núcleo (kernel): software que faz a interface entre o software e o hardware, e gerencia os recursos da máquina, como processamento, memória e periféricos   


Linus Torvalds e Linux
======================

* Na mesma época, Linus Torvalds, estudante de computação finlandês, estava criando um núcleo de sistema operacional chamado Linux 
* Inicialmente usou um licença própria, mas decidiu mudar para a GPL

.. image:: img/tux.png
    :width: 20%  


GNU/Linux
=========

* Com a integração das ferramentas GNU e o núcleo Linux, surge o sistema operacional GNU/Linux 
* Sistema operacional software livre
* Se tornou muito popular em empresas e centros de pesquisa

    * Grátis    
    * Liberdade de realizar modificações e correções

* Comunidade ativa até os dias atuais


GNU/Linux na atualidade
=======================

* Sistema operacional mais utilizado hoje em dia

    * Servidores web
    * Android
    * Sistemas embarcados (IoT)

* Versões desktop modernas e robustas

    * Perfeitamente usáveis por leigos
    * Ferramentas de acessibilidade


GNU/Linux é uma necessidade
===========================

* Para vocês que vão trabalhar com computação no futuro, mesmo que não com software livre, aprender a usar GNU/Linux é fundamental 


Impacto Social
==============

* Democratização do acesso ao conhecimento
* Privacidade e transparência
* Segurança e verificabilidade


Impacto Tecnológico
===================

* Open Source vs Software Livre
* Open Source: criado em 97 por Eric Raymond
* Netscape (Firefox)


Impacto Tecnológico
===================

* Open Source é mais produtivo
* Quase impossível começar um SO do zero
* Colaboração entre empresas
* |up| olhos, |down| bugs
* Objetivo comum


Software livre de sucesso
=========================

* Linux e Git
* Firefox, Chromium (Chrome, Brave, IE)
* OBS, Blender, VLC, Audacity, Inkscape, Gimp


Software livre de sucesso
=========================

* Linguagens de programação (Python, C, PHP)
* Framework web (Rails, Django, React, Node)
* Criptomoedas, Machine Learning, Banco de Dados


O que são distros?
==================

* Distro = Kernel Linux + programas
* Versões dos pacotes: rolling release vs point release
* Gerenciador de pacotes
* Questão visual
* Programas incluídos e programas disponíveis


O que são interfaces gráficas?
==============================

* Interface = área de trabalho, janelas, barra do sistema
* Windows |arrow| única interface
* Linux |arrow| várias opções: KDE Plasma, GNOME, XFCE4, Sway, etc
* Escolha depende de preferência pessoal: design, produtividade, etc
* Uma mesma distro pode permitir o uso de diferentes interfaces ou não


KDE Plasma
==========

.. .. image:: img/plasma.png


GNOME
=====

.. .. image:: img/gnome.png


XFCE4
=====

.. .. image:: img/xfce.png


Sway
====

.. .. image:: img/sway.png


Interfaces feias ou modernas?
=============================

* Como vimos, muitas opções
* As duas com aparência mais moderna: GNOME_ e `KDE Plasma`_. Indicadas para usuários iniciantes
* Muitos usuários experientes preferem interfaces mais simples (como o Sway)
* Mas muitos também preferem GNOME ou KDE Plasma
* Depende de caso de uso. Recursos limitados (Raspberry Pi) |arrow| interfaces leves

.. _GNOME: https://www.youtube.com/watch?v=vK-SwsWnEmo
.. _KDE Plasma: https://www.youtube.com/watch?v=ahEWG4JCA1w


Como instalar um software sem .exe?
===================================

* Gerenciadores de Pacotes (apt, yum, dnf, pacman, zypper, ...)

    * Versões graficas e em terminal
    * Aplicações já avaliadas

* Funcionamento parecido com Loja de Apps (Google Play, App Store, F-droid, ...).


É posível jogar em Linux?
=========================

* SIM!!!!
* Proton, Steam

    * https://www.protondb.com/
    * Vulkan
    * Habilitar configuração no steam

* Lutris

    * https://lutris.net/


Alternativas livres a programas comuns
======================================

* Office: LibreOffice
* Reprodução de música/vídeo: VLC
* Browser: Firefox, Chromium
* Edição de imagens: GIMP, Inkscape
* Edição de vídeos: Kdenlive, Blender


Por que contribuir?
===================

* Melhorar algo que você usa
* Aprender habilidades técnicas
* Conhecer a comunidade
* Desenvolver (soft skills)
* Viajar de graça


Contribuindo sem programar
==========================

* Documentação
* Tradução
* Reportar falhas e sugerir melhorias
* Divulgação e educação
* Design e usabilidade


Como começar?
=============

* Entre em contato!

    * Cada comunidade tem um ecosistema proprio

* Fuce no código ou documentação
* “Good first issue”
* Tente!
* Projetos de mentoria

    * GSoC, Outreachy, BOSS, LKCamp, LKMP
